plugins {
    kotlin("jvm") version "1.3.72"
    `maven-publish`
}

group = "zerocode"
version = "1.0.0"

repositories {
    mavenLocal()
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.awaitility:awaitility:4.0.3")
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "11"
    }
}

val sourcesJar by tasks.registering(Jar::class) {
    classifier = "sources"
    from(sourceSets.main.get().allSource)
}

publishing {
    publications {
        create<MavenPublication>("${project.group}-$name") {
            groupId = "${project.group}"
            artifactId = project.name
            version = "${project.version}"
            from(components["java"])
            artifact(sourcesJar.get())
        }
    }
}