package zerocode.testing

import org.awaitility.Awaitility
import java.time.Duration

fun waitUntil(atMost: Long = 5, assertion: () -> Unit) {
    Awaitility.await().atMost(Duration.ofSeconds(atMost)).untilAsserted {
        try {
            assertion()
        } catch (ex: Exception) {
            throw AssertionError(ex)
        }
    }
}